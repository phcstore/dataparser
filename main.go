package main

import (
	"dataparser/pkg/config"
	"dataparser/pkg/excel_parser"
	"flag"
	log "github.com/sirupsen/logrus"
)

func main() {
	var cfgFilePath string

	flag.StringVar(&cfgFilePath, "config", "data-parser-config.yml", "config file path")
	flag.Parse()

	log.Infof("load config file: %s", cfgFilePath)
	cfg, err := config.LoadConfig(cfgFilePath)
	if err != nil {
		log.Fatal(err)
	}
	log.Info("success load config.")

	parser := excel_parser.NewExcelParser(cfg)
	parser.ParseExcelToJson()
	log.Info("success parsing excel to json.")
}
