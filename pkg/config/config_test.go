package config

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLoadConfig(t *testing.T) {
	type args struct {
		cfgFilePath string
	}
	tests := []struct {
		name    string
		args    args
		want    *Config
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			args: args{
				cfgFilePath: "../../data-parser-config.yml",
			},
			want: &Config{
				PathConfig: &PathConfig{
					InExcelPath: "./testdata",
					OutJsonPath: "./out",
				},
				ExportConfigs: []*ExportConfig{
					{
						Name:    "HeroUnit",
						InFile:  "Hero.xlsx",
						InSheet: "HeroUnit",
						OutFile: "HeroUnit",
						ExportSingleColumns: []Column{
							{Name: "ID", Type: "int"},
							{Name: "BundleName", Type: "string"},
							{Name: "MaxLevel", Type: "int"},
							{Name: "Grade", Type: "string"},
						},
						ExportArrayColumns: nil,
					},
					{
						Name:    "HeroExp",
						InFile:  "Hero.xlsx",
						InSheet: "HeroExp",
						OutFile: "HeroExp",
						ExportSingleColumns: []Column{
							{Name: "ID", Type: "int"},
							{Name: "NeedExp", Type: "int"},
						},
						ExportArrayColumns: []Column{
							{Name: "RewardCount", Type: "int"},
						},
					},
				},
			},
			wantErr: assert.NoError,
		},
		{
			name: "failed file read",
			args: args{
				cfgFilePath: "../../none-config.yml",
			},
			want:    nil,
			wantErr: assert.Error,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := LoadConfig(tt.args.cfgFilePath)
			if !tt.wantErr(t, err, fmt.Sprintf("LoadConfig(%v)", tt.args.cfgFilePath)) {
				return
			}
			assert.Equalf(t, tt.want, got, "LoadConfig(%v)", tt.args.cfgFilePath)
		})
	}
}
