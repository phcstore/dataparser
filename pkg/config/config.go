package config

import (
	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
	"os"
)

type PathConfig struct {
	InExcelPath string `yaml:"in_excel_path"`
	OutJsonPath string `yaml:"out_json_path"`
}

type Column struct {
	Name string `yaml:"column_name"`
	Type string `yaml:"column_type"`
}

type ExportConfig struct {
	Name                string   `yaml:"name"`
	InFile              string   `yaml:"in_file"`
	InSheet             string   `yaml:"in_sheet"`
	OutFile             string   `yaml:"out_file"`
	ExportSingleColumns []Column `yaml:"export_single_columns"`
	ExportArrayColumns  []Column `yaml:"export_array_columns"`
}

type Config struct {
	PathConfig    *PathConfig     `yaml:"path"`
	ExportConfigs []*ExportConfig `yaml:"exports"`
}

func LoadConfig(cfgFilePath string) (*Config, error) {
	data, err := os.ReadFile(cfgFilePath)
	if err != nil {
		return nil, errors.Wrapf(err, "failed config file read.")
	}

	var cfg Config
	if err = yaml.Unmarshal(data, &cfg); err != nil {
		return nil, errors.Wrapf(err, "failed config file unmarshal.")
	}

	return &cfg, nil
}
