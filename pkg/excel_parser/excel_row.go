package excel_parser

import (
	"github.com/pkg/errors"
	"strconv"
	"strings"
)

type excelRow []string

func (r excelRow) isEmpty() bool {
	return len(r) < 1
}

// ====================================
// Getters
// ====================================

func (r excelRow) getInt(idx int) (int, error) {
	if len(r) <= idx {
		return 0, ErrIndexOutOfBound
	}

	value, err := strconv.ParseInt(r[idx], 10, 64)
	if err != nil {
		return 0, err
	}

	return int(value), nil
}

func (r excelRow) getIntList(idxes []int) ([]int, error) {
	values := make([]int, 0, len(idxes))

	for _, idx := range idxes {
		value, err := r.getInt(idx)
		if err != nil {
			return nil, err
		}

		values = append(values, value)
	}

	return values, nil
}

func (r excelRow) getInt64(idx int) (int64, error) {
	if len(r) <= idx {
		return 0, ErrIndexOutOfBound
	}

	value, err := strconv.ParseInt(r[idx], 10, 64)
	if err != nil {
		return 0, err
	}

	return value, nil
}

func (r excelRow) getInt64List(idxes []int) ([]int64, error) {
	values := make([]int64, 0, len(idxes))

	for _, idx := range idxes {
		value, err := r.getInt64(idx)
		if err != nil {
			return nil, err
		}

		values = append(values, value)
	}

	return values, nil
}

func (r excelRow) getFloat(idx int) (float64, error) {
	if len(r) <= idx {
		return 0, ErrIndexOutOfBound
	}

	value, err := strconv.ParseFloat(r[idx], 64)
	if err != nil {
		return 0, err
	}

	return value, nil
}

func (r excelRow) getFloatList(idxes []int) ([]float64, error) {
	values := make([]float64, 0, len(idxes))

	for _, idx := range idxes {
		value, err := r.getFloat(idx)
		if err != nil {
			return nil, err
		}

		values = append(values, value)
	}

	return values, nil
}

func (r excelRow) getString(idx int) (string, error) {
	if len(r) <= idx {
		return "", ErrIndexOutOfBound
	}

	value := strings.TrimSpace(r[idx])

	return value, nil
}

func (r excelRow) getStringList(idxes []int) ([]string, error) {
	values := make([]string, 0, len(idxes))

	for _, idx := range idxes {
		value, err := r.getString(idx)
		if err != nil {
			return nil, err
		}

		values = append(values, value)
	}

	return values, nil
}

// ====================================
// Find Column Index
// ====================================

func (r excelRow) findColumnIdx(name string) (int, error) {
	for idx, value := range r {
		columnName := strings.TrimSpace(value)

		if name == columnName {
			return idx, nil
		}
	}

	return 0, errors.Wrapf(ErrNotFoundColumnInHeader, "single columnName: %s", name)
}

func (r excelRow) findColumnIdxes(name string) ([]int, error) {
	var idxes []int

	findPrefix := name + "_"

	for idx, value := range r {
		columnName := strings.TrimSpace(value)

		if strings.HasPrefix(columnName, findPrefix) {
			idxes = append(idxes, idx)
		}
	}

	if len(idxes) < 1 {
		return nil, errors.Wrapf(ErrNotFoundColumnInHeader, "array columnName: %s", name)
	}

	return idxes, nil
}
