package excel_parser

import (
	"dataparser/pkg/config"
)

type HeaderElement struct {
	columnName       string
	columnType       string
	isArray          bool
	singleColumnIdx  int
	arrayColumnIdxes []int
}

type Header struct {
	elementMap map[string]*HeaderElement
}

func CreateHeader(exportCfg *config.ExportConfig, headerRow excelRow) (*Header, error) {
	headerBuilder := NewHeaderBuilder().Init()

	for _, column := range exportCfg.ExportSingleColumns {
		columnIdx, err := headerRow.findColumnIdx(column.Name)
		if err != nil {
			return nil, err
		}

		element := &HeaderElement{
			columnName:       column.Name,
			columnType:       column.Type,
			isArray:          false,
			singleColumnIdx:  columnIdx,
			arrayColumnIdxes: nil,
		}

		headerBuilder.AppendElement(element)
	}

	for _, column := range exportCfg.ExportArrayColumns {
		columnIdxes, err := headerRow.findColumnIdxes(column.Name)
		if err != nil {
			return nil, err
		}

		element := &HeaderElement{
			columnName:       column.Name,
			columnType:       column.Type,
			isArray:          true,
			singleColumnIdx:  0,
			arrayColumnIdxes: columnIdxes,
		}

		headerBuilder.AppendElement(element)
	}

	return headerBuilder.Build(), nil
}

func (h *Header) GetElementMap() map[string]*HeaderElement {
	return h.elementMap
}

type HeaderBuilder struct {
	header *Header
}

func NewHeaderBuilder() HeaderBuilder {
	return HeaderBuilder{}
}

func (b HeaderBuilder) Init() HeaderBuilder {
	b.header = &Header{
		elementMap: make(map[string]*HeaderElement),
	}

	return b
}

func (b HeaderBuilder) SetElementMap(elementMap map[string]*HeaderElement) HeaderBuilder {
	b.header.elementMap = elementMap

	return b
}

func (b HeaderBuilder) AppendElement(element *HeaderElement) HeaderBuilder {
	b.header.elementMap[element.columnName] = element

	return b
}

func (b HeaderBuilder) Build() *Header {
	return b.header
}
