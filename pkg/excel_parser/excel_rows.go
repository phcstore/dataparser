package excel_parser

import (
	"strings"
)

type excelRows []excelRow

func (r excelRows) findHeaderRow() (excelRow, error) {
	for _, row := range r {
		if row.isEmpty() {
			continue
		}

		headColumn := strings.TrimSpace(row[0])

		// key row 체크
		if headColumn == SymbolHeaderRow {
			return row, nil
		}
	}

	return nil, ErrNotFoundHeaderRow
}

func (r excelRows) nextRow(idx int) (excelRow, int) {
	for ; len(r) > idx; idx++ {
		row := r[idx]

		if row.isEmpty() {
			continue
		}

		headColumn := strings.TrimSpace(row[0])

		// checked ignore excelRow
		switch headColumn {
		case SymbolHeaderRow, SymbolIgnoreRow:
			continue
		}

		return row, idx
	}

	return nil, 0
}
