package excel_parser

import (
	"dataparser/pkg/config"
	"github.com/pkg/errors"
	"github.com/xuri/excelize/v2"
	"path/filepath"
)

const (
	ColumnTypeInt    = "int"
	ColumnTypeInt64  = "int64"
	ColumnTypeFloat  = "float"
	ColumnTypeString = "string"
)

const (
	SymbolHeaderRow = "~"
	SymbolIgnoreRow = "//"
)

type ExcelReader struct {
	filePath  string
	exportCfg *config.ExportConfig

	excelRows   excelRows
	curExcelRow excelRow
	curReadIdx  int
	nextReadIdx int
}

func NewExcelReader(filePath string, exportCfg *config.ExportConfig) *ExcelReader {
	reader := &ExcelReader{
		filePath:    filePath,
		exportCfg:   exportCfg,
		curReadIdx:  0,
		nextReadIdx: 0,
	}

	return reader
}

func (r *ExcelReader) openExcelFile(filePath, fileName string) (*excelize.File, error) {
	excelFilePath := filepath.Join(filePath, fileName)
	file, err := excelize.OpenFile(excelFilePath)
	defer func() {
		_ = file.Close()
	}()

	if err != nil {
		return nil, err
	}

	return file, nil
}

func (r *ExcelReader) next() bool {
	row, idx := r.excelRows.nextRow(r.nextReadIdx)
	if row == nil {
		return false
	}

	r.curExcelRow = row
	r.curReadIdx = idx
	r.nextReadIdx = idx + 1

	return true
}

func (r *ExcelReader) read(header *Header) (map[string]interface{}, error) {
	line := make(map[string]interface{})
	for _, element := range header.GetElementMap() {
		var value interface{}
		var err error

		switch element.columnType {
		case ColumnTypeInt:
			if element.isArray {
				value, err = r.curExcelRow.getIntList(element.arrayColumnIdxes)
			} else {
				value, err = r.curExcelRow.getInt(element.singleColumnIdx)
			}
		case ColumnTypeInt64:
			if element.isArray {
				value, err = r.curExcelRow.getInt64List(element.arrayColumnIdxes)
			} else {
				value, err = r.curExcelRow.getInt64(element.singleColumnIdx)
			}
		case ColumnTypeFloat:
			if element.isArray {
				value, err = r.curExcelRow.getFloatList(element.arrayColumnIdxes)
			} else {
				value, err = r.curExcelRow.getFloat(element.singleColumnIdx)
			}
		case ColumnTypeString:
			if element.isArray {
				value, err = r.curExcelRow.getStringList(element.arrayColumnIdxes)
			} else {
				value, err = r.curExcelRow.getString(element.singleColumnIdx)
			}
		}

		if err != nil {
			return nil, errors.Wrapf(err, "Column: %s", element.columnName)
		}

		line[element.columnName] = value
	}

	return line, nil
}

func (r *ExcelReader) ReadFile() ([]interface{}, error) {
	var formattedList []interface{}

	file, err := r.openExcelFile(r.filePath, r.exportCfg.InFile)
	if err != nil {
		return nil, err
	}

	rows, err := file.GetRows(r.exportCfg.InSheet)
	if err != nil {
		return nil, err
	}

	for _, row := range rows {
		r.excelRows = append(r.excelRows, row)
	}

	headerRow, err := r.excelRows.findHeaderRow()
	if err != nil {
		return nil, err
	}

	header, err := CreateHeader(r.exportCfg, headerRow)
	if err != nil {
		return nil, err
	}

	for r.next() {
		line, err := r.read(header)
		if err != nil {
			return nil, errors.Wrapf(err, "Row: %d", r.curReadIdx)
		}

		formattedList = append(formattedList, line)
	}

	return formattedList, nil
}
