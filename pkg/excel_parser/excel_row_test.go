package excel_parser

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

func Test_excelRow_isEmpty(t *testing.T) {
	type fields struct {
		excelRow excelRow
	}

	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "Empty",
			fields: fields{
				excelRow: excelRow{},
			},
			want: true,
		},
		{
			name: "NotEmpty",
			fields: fields{
				excelRow: excelRow{"A"},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.fields.excelRow.isEmpty()
			require.Equal(t, tt.want, got)
		})
	}
}

func Test_excelRow_getInt(t *testing.T) {
	type fields struct {
		excelRow excelRow
	}
	type args struct {
		idx int
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			fields: fields{
				excelRow: excelRow{"1", "2"},
			},
			args: args{
				idx: 1,
			},
			want:    2,
			wantErr: assert.NoError,
		},
		{
			name: "OutOfBound",
			fields: fields{
				excelRow: excelRow{"1", "2"},
			},
			args: args{
				idx: 2,
			},
			want:    0,
			wantErr: assert.Error,
		},
		{
			name: "NotInt",
			fields: fields{
				excelRow: excelRow{"1", "A"},
			},
			args: args{
				idx: 1,
			},
			want:    0,
			wantErr: assert.Error,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.fields.excelRow.getInt(tt.args.idx)
			tt.wantErr(t, err)
			require.Equal(t, tt.want, got)
		})
	}
}

func Test_excelRow_getIntList(t *testing.T) {
	type fields struct {
		excelRow excelRow
	}
	type args struct {
		idxes []int
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []int
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			fields: fields{
				excelRow: excelRow{"1", "2", "3"},
			},
			args: args{
				idxes: []int{1, 2},
			},
			want:    []int{2, 3},
			wantErr: assert.NoError,
		},
		{
			name: "OutOfBound",
			fields: fields{
				excelRow: excelRow{"1", "2", "3"},
			},
			args: args{
				idxes: []int{2, 3},
			},
			want:    nil,
			wantErr: assert.Error,
		},
		{
			name: "NotInt",
			fields: fields{
				excelRow: excelRow{"1", "2", "A"},
			},
			args: args{
				idxes: []int{2, 3},
			},
			want:    nil,
			wantErr: assert.Error,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.fields.excelRow.getIntList(tt.args.idxes)
			tt.wantErr(t, err)
			require.Equal(t, tt.want, got)
		})
	}
}

func Test_excelRow_findColumnIdx(t *testing.T) {
	type fields struct {
		excelRow excelRow
	}
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			fields: fields{
				excelRow: excelRow{"ID", "BundleName"},
			},
			args: args{
				name: "ID",
			},
			want:    0,
			wantErr: assert.NoError,
		},
		{
			name: "fail - not found",
			fields: fields{
				excelRow: excelRow{"ID", "BundleName"},
			},
			args: args{
				name: "None",
			},
			want:    0,
			wantErr: assert.Error,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.fields.excelRow.findColumnIdx(tt.args.name)
			if !tt.wantErr(t, err, fmt.Sprintf("findColumnIdx(%v)", tt.args.name)) {
				return
			}
			assert.Equalf(t, tt.want, got, "findColumnIdx(%v)", tt.args.name)
		})
	}
}

func Test_excelRow_findColumnIdxes(t *testing.T) {
	type fields struct {
		excelRow excelRow
	}
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []int
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			fields: fields{
				excelRow: excelRow{"ID", "RewardCount_1", "RewardCount_2"},
			},
			args: args{
				name: "RewardCount",
			},
			want:    []int{1, 2},
			wantErr: assert.NoError,
		},
		{
			name: "fail - not found",
			fields: fields{
				excelRow: excelRow{"ID", "None_1", "None_2"},
			},
			args: args{
				name: "RewardCount",
			},
			want:    nil,
			wantErr: assert.Error,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.fields.excelRow.findColumnIdxes(tt.args.name)
			if !tt.wantErr(t, err, fmt.Sprintf("findColumnIdxes(%v)", tt.args.name)) {
				return
			}
			assert.Equalf(t, tt.want, got, "findColumnIdxes(%v)", tt.args.name)
		})
	}
}
