package excel_parser

import "github.com/pkg/errors"

var (
	ErrNotFoundHeaderRow      = errors.New("not found Header excelRow.")
	ErrNotFoundColumnInHeader = errors.New("not found Header column.")
	ErrIndexOutOfBound        = errors.New("index out of bound.")
)
