package excel_parser

import (
	"dataparser/pkg/config"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCreateHeader(t *testing.T) {
	exportCfg := &config.ExportConfig{
		ExportSingleColumns: []config.Column{
			{Name: "ID", Type: "int"},
			{Name: "BundleName", Type: "string"},
		},
		ExportArrayColumns: []config.Column{
			{Name: "RewardCount", Type: "int"},
		},
	}

	type args struct {
		exportCfg *config.ExportConfig
		headerRow excelRow
	}
	tests := []struct {
		name    string
		args    args
		want    *Header
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			args: args{
				exportCfg: exportCfg,
				headerRow: excelRow{"ID", "BundleName", "RewardCount_1", "RewardCount_2"},
			},
			want: NewHeaderBuilder().Init().
				AppendElement(&HeaderElement{
					columnName:       "ID",
					columnType:       "int",
					isArray:          false,
					singleColumnIdx:  0,
					arrayColumnIdxes: nil,
				}).
				AppendElement(&HeaderElement{
					columnName:       "BundleName",
					columnType:       "string",
					isArray:          false,
					singleColumnIdx:  1,
					arrayColumnIdxes: nil,
				}).
				AppendElement(&HeaderElement{
					columnName:       "RewardCount",
					columnType:       "int",
					isArray:          true,
					singleColumnIdx:  0,
					arrayColumnIdxes: []int{2, 3},
				}).
				Build(),
			wantErr: assert.NoError,
		},
		{
			name: "fail - not found single column",
			args: args{
				exportCfg: exportCfg,
				headerRow: excelRow{"ID", "RewardCount_1", "RewardCount_2"},
			},
			want:    nil,
			wantErr: assert.Error,
		},
		{
			name: "fail - not found array column",
			args: args{
				exportCfg: exportCfg,
				headerRow: excelRow{"ID", "BundleName", "None_1", "None_2"},
			},
			want:    nil,
			wantErr: assert.Error,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := CreateHeader(tt.args.exportCfg, tt.args.headerRow)
			if !tt.wantErr(t, err, fmt.Sprintf("CreateHeader(%v, %v)", tt.args.exportCfg, tt.args.headerRow)) {
				return
			}
			assert.Equalf(t, tt.want, got, "CreateHeader(%v, %v)", tt.args.exportCfg, tt.args.headerRow)
		})
	}
}
