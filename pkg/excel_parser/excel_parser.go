package excel_parser

import (
	"dataparser/pkg/config"
	"encoding/json"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"os"
	"path/filepath"
	"sync"
)

type ExcelParser struct {
	cfg *config.Config
}

func NewExcelParser(cfg *config.Config) *ExcelParser {
	return &ExcelParser{
		cfg: cfg,
	}
}

func (p *ExcelParser) ParseExcelToJson() {
	var failErrs []error

	collectingFailErrCh := make(chan error)
	defer close(collectingFailErrCh)

	var wg sync.WaitGroup

	for _, exportCfg := range p.cfg.ExportConfigs {
		wg.Add(1)
		go func(exportCfg *config.ExportConfig) {
			defer wg.Done()

			if err := p.execute(exportCfg); err != nil {
				collectingFailErrCh <- errors.Wrapf(err,
					"ExcelFile(%s), Sheet(%s)",
					exportCfg.InFile, exportCfg.InSheet)
				return
			}

			logrus.Infof("Export ... ExcelFile(%s), Sheet(%s)",
				exportCfg.InFile, exportCfg.InSheet)
		}(exportCfg)
	}

	// collect fail error
	go func() {
		for {
			select {
			case failErr := <-collectingFailErrCh:
				failErrs = append(failErrs, failErr)
			}
		}
	}()

	wg.Wait()

	if len(failErrs) > 0 {
		for _, err := range failErrs {
			logrus.Errorf("ExportFail ... %v", err)
		}
	}
}

func (p *ExcelParser) execute(exportCfg *config.ExportConfig) error {
	reader := NewExcelReader(p.cfg.PathConfig.InExcelPath, exportCfg)

	parsedData, err := reader.ReadFile()
	if err != nil {
		return err
	}

	marshalData, err := json.MarshalIndent(parsedData, "", "  ")
	if err != nil {
		return err
	}

	if err = os.MkdirAll(p.cfg.PathConfig.OutJsonPath, os.ModePerm); err != nil {
		return err
	}

	outputFilePath := filepath.Join(p.cfg.PathConfig.OutJsonPath, exportCfg.OutFile+".json")
	if err = os.WriteFile(outputFilePath, marshalData, 0644); err != nil {
		return err
	}

	return nil
}
