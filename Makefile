build:
	GOOS=darwin GOARCH=amd64 go build -o bin/data-parser main.go
	GOOS=windows GOARCH=amd64 go build -o bin/data-parser.exe main.go

data-parser: ## 엑셀을 파싱하여 json 파일로 변환
	go run main.go -config ./data-parser-config.yml